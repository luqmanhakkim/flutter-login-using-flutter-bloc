import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginInitial extends LoginEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoginButtonPressed extends LoginEvent{
  final String username;
  final String password;

  const LoginButtonPressed({this.username, this.password});

  @override
  // TODO: implement props
  List<Object> get props => [username, password];

  @override
  String toString() => 'LoginButtonPressed {username: $username, password: $password}';



}