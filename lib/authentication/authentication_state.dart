import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();
}

class InitialAuthenticationState extends AuthenticationState {
  @override
  List<Object> get props => [];
}

///If authentication unitialized user see splash screen
class AuthenticationUninitialized extends AuthenticationState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

///If authentication authenticated user see home screen
class AuthenticationAuthenticated extends AuthenticationState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

///If authentication unauthenticated user see login screen
class AuthenticationUnauthenticated extends AuthenticationState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

///If authentication loading user see progress indicator
class AuthenticationLoading extends AuthenticationState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}
