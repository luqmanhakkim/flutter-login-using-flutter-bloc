import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();
}

///Notify bloc to check user whether it was authenticated or not
class AppStarted extends AuthenticationEvent {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

///Notify bloc that user successfully login in
class LoggedIn extends AuthenticationEvent {
  final String token;

  const LoggedIn({@required this.token});

  @override
  // TODO: implement props
  List<Object> get props => [token];

  @override
  String toString() => 'LoggedIn {token: $token}';
}

///Notify bloc that user is now log out
class LoggedOut extends AuthenticationEvent{
  @override
  // TODO: implement props
  List<Object> get props => null;

}
